#include <iostream>

#include "Report.hpp"
#include "DirToList.hpp"
#include "Sort.hpp"
#include "Stats.hpp"
#include "ListUtils.hpp"


// include other header files as needed


int main(void) {
	Report rpt;

	//Creates a single linked list of EMployment objects from the selected directory
	Employment *head = DirToList("database");
	
	//The rest of main fills in and prints the Report object
	rpt.num_areas = list_length(head);
	
	//Sorts list by total annual wages
	head = sort_empl_by_total_annual_wages(head);

	rpt.total_wages = total_annual_wages(head);
	rpt.min_wages = min_annual_wages(head);
	rpt.med_wages = med_annual_wages(head);
	rpt.max_wages = max_annual_wages(head);

	//Sorts list by total annual employment level
	head = sort_empl_by_annual_avg_emplvl(head);

	rpt.total_emplvl = total_annual_emplvl(head);
	rpt.min_emplvl = min_annual_emplvl(head);
	rpt.med_emplvl = med_annual_emplvl(head);
	rpt.max_emplvl = max_annual_emplvl(head);

	std::cout << rpt << std::endl;
	
	cleanup_list(head);
}
