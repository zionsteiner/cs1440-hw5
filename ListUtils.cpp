#include "ListUtils.hpp"
#include <fstream>
#include <iostream>

//Builds linked list of Employment objects from given file
Employment* build_empl_list(std::string filename) {
    //Open file and skip first line
    std::ifstream fin(filename);
    {
        std::string firstLine;
        getline(fin, firstLine);
    }
    
    //Member variables of Employment
    std::string area_fips;
    unsigned annual_avg_emplvl;
    long unsigned total_annual_wages;
    Employment* head = NULL;
    
    //Links objects until end of file
    while(fin >> area_fips) {
        fin >> annual_avg_emplvl;
        fin >> total_annual_wages;
        
        add_employment_data(head, area_fips, annual_avg_emplvl, total_annual_wages);
    }
    
    return head;
}

//Creates new Employment object and adds it to the top of the list
void add_employment_data(Employment*& head, std::string area_fips, unsigned annual_avg_emplvl, long unsigned total_annual_wages) {
    Employment* newListEntry = new Employment(area_fips, annual_avg_emplvl, total_annual_wages, head);
    head = newListEntry;
}

//Connects two linked lists
void append_lists(Employment* head, Employment *tail) {
    while (head->next != NULL) {
        head = head->next;
    }
    head->next = tail;
}

//Returns the count of entries in the list
int list_length(Employment *emp) {
    int itemCount = 0;
    while (emp != NULL) {
        itemCount++;
        emp = emp->next;
    }
    return itemCount;
}

//Prints
void print_every_empl(Employment *emp) {
    int itemCount = list_length(emp);
    for (int i = 0; i < itemCount; i++) {
    std::cout << *emp;
    
    }
}

//Deletes linked list
void cleanup_list(Employment* head) {
    Employment* temp = NULL;    //used to store and delete current head while current head is pointed to next Employment
    
    while (head->next != NULL) {
        temp = head;
        head = head->next;
        delete temp;
    }
    delete head;
}