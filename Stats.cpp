#include <limits.h> // provides UINT_MAX, ULONG_MAX

#include "Stats.hpp"
#include "ListUtils.hpp"



unsigned long total_annual_wages(Employment *emp) {
    int itemCount = 0;
    unsigned long wagesSum = 0;
    while (emp != NULL) {
        itemCount++;
        wagesSum += emp->total_annual_wages;
        emp = emp->next;
    }
    
    return wagesSum;
}


unsigned long min_annual_wages(Employment *emp) {
    return emp->total_annual_wages;
}


// Assume that the list has been sorted appropriately
unsigned long med_annual_wages(Employment *emp) {
    int medPosition = list_length(emp)/2;
    for (int i = 0; i < medPosition; i++) {
        emp = emp->next;
    }
    
    return emp->total_annual_wages;
}


unsigned long max_annual_wages(Employment *emp) {
    while (emp->next != NULL) {
        emp = emp->next;
    }
    
    return emp->total_annual_wages;
}


unsigned total_annual_emplvl(Employment *emp) {
    int itemCount = 0;
    unsigned long emplvlSum = 0;
    while (emp != NULL) {
        itemCount++;
        emplvlSum += emp->annual_avg_emplvl;
        emp = emp->next;
    }
    
    return emplvlSum;
}


unsigned min_annual_emplvl(Employment *emp) {
    return emp->annual_avg_emplvl;
}


// Assume that the list has been sorted appropriately
unsigned med_annual_emplvl(Employment *emp) {
    int medPosition = list_length(emp)/2;
    for (int i = 0; i < medPosition; i++) {
        emp = emp->next;
    }
    
    return emp->annual_avg_emplvl;
}


unsigned max_annual_emplvl(Employment *emp) {
    while (emp->next != NULL) {
        emp = emp->next;
    }
    
    return emp->annual_avg_emplvl;
}